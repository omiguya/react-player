const convertFormatedTimeToSeconds = (time) => {
    return time.split(':').reduce((acc, time) => (60 * acc) + +time);
}

const convertSecondsToHHMMss = (totalSeconds) => {
    const sec_num = parseInt(totalSeconds, 10);
    let hours = Math.floor(sec_num / 3600);
    let minutes = Math.floor((sec_num - (hours * 3600)) / 60);
    let seconds = sec_num - (hours * 3600) - (minutes * 60);
  
    if (hours < 10) { hours = "0" + hours; }
    if (minutes < 10) { minutes = "0" + minutes; }
    if (seconds < 10) { seconds = "0" + seconds; }
    
    if( hours === '00' )  {
        return `${minutes}:${seconds}`;    
    } else {
        return `${hours}:${minutes}:${seconds}`;
    }

    
  }
  
const percent = (current, total) => {
    return (current / total) * 100
}

const enterFullScreen = (element) => {
    if (element.requestFullscreen) {
        element.requestFullscreen();
    } else if (element.msRequestFullscreen) {
        element.msRequestFullscreen();
    } else if (element.mozRequestFullScreen) {
        element.mozRequestFullScreen();
    } else if (element.webkitRequestFullscreen) {
        element.webkitRequestFullscreen();
    }
}
  
const exitFullScreen = () => {
    if (document.exitFullscreen) {
        document.exitFullscreen();
    } else if (document.msExitFullscreen) {
        document.msExitFullscreen();
    } else if (document.mozCancelFullScreen) {
        document.mozCancelFullScreen();
    } else if (document.webkitExitFullscreen) {
        document.webkitExitFullscreen();
    }
}

function defineStepStateByTime(time, step, state) {
    if (time >= step.start && time < step.end) {
        this.setState({
              
        })
        state = state.question;
    }
    if (step.repeat && step.repeat.length === 2 && time >= step.repeat[0] && time < step.repeat[1]) {
        state = state.waiting;
    }
    if (step.invalidAnswerTime && step.invalidAnswerTime.length === 2 && time >= step.invalidAnswerTime[0] && time < step.invalidAnswerTime[1]) {
        state = state.invalid;
    }
    if (step.validAnswerTime && time >= step.validAnswerTime) {
        state = state.valid;
    }
    return state;
}

function isInvalidAnswerEnds(time, step) {
    return step.invalidAnswerTime && step.invalidAnswerTime.length === 2 && time >= step.invalidAnswerTime[1];
}

function repeatWaitingIfNeeded(time, step, player) {
    if (step.repeat && step.repeat.length === 2 && time >= step.repeat[1]) {
        player.currentTime = step.repeat[0];
    }
}

export {
    convertFormatedTimeToSeconds,
    convertSecondsToHHMMss,
    percent,
    enterFullScreen, 
    exitFullScreen,
    defineStepStateByTime,
    isInvalidAnswerEnds,
    repeatWaitingIfNeeded
}