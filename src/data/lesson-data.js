function LessonData() {
  this.assetsPath = './assets';
  this.soundsPath = './assets/sf';
  this.videoSource = 'https://storage.yandexcloud.net/skillclass-interactive-videos--demo/video_demo.mp4';
  this.posterUrl = './assets/video-preview.png';

  this.getTimelineData = function(rootPath, sound) {
    var labelAnimation = {
      onReady: {
        opacity: 0,
        translateY: -10,
      },
      onStart: {
        duration: 300,
        opacity: 1,
        translateY: 0,
        easing: "easeInOutQuad",
        begin: function() {
          sound.playIntro();
        }
      },
      onEnd: {
        opacity: 0,
        translateY: -10,
      },
    };
    
    var buttonAnimation = {
      onReady: {
        scale: 0.5,
        opacity: 0,
      },
      onStart: {
        duration: 800,
        delay: 400,
        scale: 1.0,
        opacity: 1,
        begin: function() {
          sound.playTick();
        }
      },
      onEndValid: {
        scale: 0.5,
        opacity: 0,
      },
      onClick: {
        duration: 100,
        scale: 0.99,
        direction: 'alternate',
        easing: 'easeInOutCubic',
      },
      onStart2: {
        duration: 800,
        delay: 550,
        scale: 1.0,
        opacity: 1,
        begin: function() {
          sound.playTick();
        }
      },
      onEndDefault: {
        opacity: 0,
        duration: 2000,
      },
    }
    
    return [
      // Q1
      {
        time: 38.53,
        end: 47.05,
        repeat: [39.13, 42.25],
        invalidAnswerTime: [43.25, 46.05],
        validAnswerTime: 47.05,
        tag: "Q1",
        scene: [
          {
            class: 'skillclass__controls_container',
            top: '0',
            left: '0',
            right: '0',
            bottom: '75%',
            children: [
              {
                img: rootPath + '/scene-label_question-travel.png',
                class: 'skillclass__controls_title',
                width: '80%',
                onReady: labelAnimation.onReady,
                onStart: labelAnimation.onStart,
                onEnd:   labelAnimation.onEnd,
              }
            ]
          },
          {
            class: 'skillclass__controls_container',
            bottom: '10%',
            top: '50%',
            left: '0',
            right: '0',
            children: [
              {
                img: rootPath + '/button_map.png',
                class: 'skillclass__controls_button skillclass__controls_button--blue-glow',
                width: '35%',
                action: StepStatus.valid,
                onReady: buttonAnimation.onReady,
                onStart: buttonAnimation.onStart,
                onEnd: buttonAnimation.onEndValid,
                onclick: buttonAnimation.onClick,
                tag: "1. Map",
              },
              {
                img: rootPath + '/button_camera.png',
                class: 'skillclass__controls_button skillclass__controls_button--blue-glow',
                action: StepStatus.invalid,
                width: '35%',
                onReady: buttonAnimation.onReady,
                onStart: buttonAnimation.onStart2,
                onEnd: buttonAnimation.onEndDefault,
                onclick: buttonAnimation.onClick,
                tag: "2. Camera",
              },
            ]
          },
        ]
      },
    
      // Q2
      {
        time: 64.53,
        end: 74.216,
        repeat: [65.03, 73.216],
        validAnswerTime: 74.216,
        invalidAnswerTime: [0, 0.1],
        tag: "Q2",
        scene: [
          {
            class: 'skillclass__controls_container',
            top: '0',
            left: '0',
            right: '0',
            bottom: '75%',
            children: [
              {
                img: rootPath + '/scene-label_open-case.png',
                class: 'skillclass__controls_title',
                width: '80%',
                onReady: labelAnimation.onReady,
                onStart: labelAnimation.onStart,
                onEnd:   labelAnimation.onEnd,
              }
            ]
          },
          {
            class: 'skillclass__controls_container',
            bottom: '15%',
            top: '70%',
            left: '0',
            right: '0',
            children: [
              {
                img: rootPath + '/button_open.png',
                class: 'skillclass__controls_button skillclass__controls_button--green-glow',
                action: StepStatus.valid,
                width: '30%',
                onReady: buttonAnimation.onReady,
                onStart: buttonAnimation.onStart,
                onEnd: buttonAnimation.onEndValid,            
                onclick: {
                  duration: 100,
                  scale: 0.99,
                  // direction: 'alternate',
                  easing: 'easeInOutCubic',
                  begin: function() {
                    sound.playCelebration();
                  }
                },
                tag: "Open case",
              }
            ]
          }
        ],
      },
    
      // Q3
      {
        time: 101.23,
        end: 116.46,
        repeat: [108.016, 112.46],
        invalidAnswerTime: [113.46, 115.46],
        validAnswerTime: 116.46,
        tag: "Q3",
        scene: [
          {
            class: 'skillclass__controls_container',
            top: '0',
            left: '0',
            right: '0',
            bottom: '75%',
            children: [
              {
                img: rootPath + '/scene-label_question-pyramid.png',
                class: 'skillclass__controls_title',
                width: '80%',
                onReady: labelAnimation.onReady,
                onStart: labelAnimation.onStart,
                onEnd:   labelAnimation.onEnd,
              }
            ]
          },
          {
            class: 'skillclass__controls_container',
            bottom: '15%',
            top: '55%',
            left: '0',
            right: '0',
            children: [
              {
                img: rootPath + '/button_pyramid.png',
                class: 'skillclass__controls_button skillclass__controls_button--blue-glow',
                action: StepStatus.valid,
                width: '30%',
                onReady: buttonAnimation.onReady,
                onStart: buttonAnimation.onStart,
                onEnd: buttonAnimation.onEndValid,
                onclick: buttonAnimation.onClick,
                tag: "1. Egypt pyramids",
              },
              {
                img: rootPath + '/button_eiffel.png',
                class: 'skillclass__controls_button skillclass__controls_button--blue-glow',
                action: StepStatus.invalid,
                width: '30%',
                onReady: buttonAnimation.onReady,
                onStart: buttonAnimation.onStart2,
                onEnd: buttonAnimation.onEndDefault,
                onclick: buttonAnimation.onClick,
                tag: "2. Eiffel tower",
              },
            ]
          },
        ]
      },
    
      // Q4.1
      {
        time: 344.68,
        end: 354.083,
        repeat: [345.183, 348.33],
        invalidAnswerTime: [349.33, 353.083],
        validAnswerTime: 354.083,
        tag: "Q4.1",
        scene: [
          {
            class: 'skillclass__controls_container',
            top: '0',
            left: '0',
            right: '0',
            bottom: '75%',
            children: [
              {
                img: rootPath + '/scene-label_question-glyph.png',
                class: 'skillclass__controls_title',
                width: '80%',
                onReady: labelAnimation.onReady,
                onStart: labelAnimation.onStart,
                onEnd:   labelAnimation.onEnd,
              }
            ]
          },
          {
            class: 'skillclass__controls_container',
            bottom: '15%',
            top: '70%',
            left: '0',
            right: '0',
            children: [
              {
                img: rootPath + '/button_take-rest.png',
                class: 'skillclass__controls_button skillclass__controls_button--yellow-glow  skillclass__controls_button--glow-md',
                action: StepStatus.invalid,
                width: '30%',
                onReady: buttonAnimation.onReady,
                onStart: buttonAnimation.onStart,
                onEnd: buttonAnimation.onEndDefault,
                onclick: buttonAnimation.onClick,
                tag: "1. To rest",
              },
              {
                img: rootPath + '/button_to-eat.png',
                class: 'skillclass__controls_button skillclass__controls_button--yellow-glow  skillclass__controls_button--glow-md',
                action: StepStatus.valid,
                width: '30%',
                onReady: buttonAnimation.onReady,
                onStart: buttonAnimation.onStart2,
                onEnd: buttonAnimation.onEndValid,
                onclick: buttonAnimation.onClick,
                tag: "2. To eat",
              },
            ]
          },
        ]
      },
    
      // Q4.2
      {
        time: 362.73,
        end: 376.16,
        repeat: [363.23, 370.45],
        invalidAnswerTime: [371.45, 375.16],
        validAnswerTime: 376.16,
        tag: "Q4.2",
        scene: [
          {
            class: 'skillclass__controls_container',
            top: '0',
            left: '0',
            right: '0',
            bottom: '75%',
            children: [
              {
                img: rootPath + '/scene-label_question-glyph.png',
                class: 'skillclass__controls_title',
                width: '80%',
                onReady: labelAnimation.onReady,
                onStart: labelAnimation.onStart,
                onEnd:   labelAnimation.onEnd,
              }
            ]
          },
          {
            class: 'skillclass__controls_container',
            bottom: '15%',
            top: '70%',
            left: '0',
            right: '0',
            children: [
              {
                img: rootPath + '/button_take-shower.png',
                class: 'skillclass__controls_button skillclass__controls_button--yellow-glow  skillclass__controls_button--glow-md',
                action: StepStatus.valid,
                width: '30%',
                onReady: buttonAnimation.onReady,
                onStart: buttonAnimation.onStart,
                onEnd: buttonAnimation.onEndValid,
                onclick: buttonAnimation.onClick,
                tag: "1. Take a shower",
              },
              {
                img: rootPath + '/button_to-think.png',
                class: 'skillclass__controls_button skillclass__controls_button--yellow-glow  skillclass__controls_button--glow-md',
                action: StepStatus.invalid,
                width: '30%',
                onReady: buttonAnimation.onReady,
                onStart: buttonAnimation.onStart2,
                onEnd: buttonAnimation.onEndDefault,
                onclick: buttonAnimation.onClick,
                tag: "2. To think",
              },
              
            ]
          },
        ]
      },
    
      // Q4.3
      {
        time: 383.916,
        end: 394.28,
        repeat: [384.816, 390.1],
        invalidAnswerTime: [391.1, 393.28],
        validAnswerTime: 394.28,
        tag: "Q4.3",
        scene: [
          {
            class: 'skillclass__controls_container',
            top: '0',
            left: '0',
            right: '0',
            bottom: '75%',
            children: [
              {
                img: rootPath + '/scene-label_question-glyph.png',
                class: 'skillclass__controls_title',
                width: '80%',
                onReady: labelAnimation.onReady,
                onStart: labelAnimation.onStart,
                onEnd:   labelAnimation.onEnd,
              }
            ]
          },
          {
            class: 'skillclass__controls_container',
            bottom: '15%',
            top: '70%',
            left: '0',
            right: '0',
            children: [
              {
                img: rootPath + '/button_do-work.png',
                class: 'skillclass__controls_button skillclass__controls_button--yellow-glow  skillclass__controls_button--glow-md',
                action: StepStatus.valid,
                width: '30%',
                onReady: buttonAnimation.onReady,
                onStart: buttonAnimation.onStart,
                onEnd: buttonAnimation.onEndValid,
                onclick: buttonAnimation.onClick,
                tag: "1. Do work",
              },
              {
                img: rootPath + '/button_to-hunt.png',
                class: 'skillclass__controls_button skillclass__controls_button--yellow-glow  skillclass__controls_button--glow-md',
                action: StepStatus.invalid,
                width: '30%',
                onReady: buttonAnimation.onReady,
                onStart: buttonAnimation.onStart2,
                onEnd: buttonAnimation.onEndDefault,
                onclick: buttonAnimation.onClick,
                tag: "2. To hunt",
              },
            ]
          },
        ]
      },
  
      // Finish
      {
        time: 414,
        end: 416,
        repeat: [414.5, 415],
        invalidAnswerTime: [415, 415],
        validAnswerTime: 416,
        tag: "Finish",
        scene: [
          {
            class: 'skillclass__controls_container',
            top: '15%',
            left: '5%',
            right: '5%',
            bottom: '15%',
            children: [
              {
                img: rootPath + '/scene-panel_finish.png',
                class: 'skillclass__controls_title',
                width: '90%',
                onReady: labelAnimation.onReady,
                onStart: labelAnimation.onStart,
                onEnd:   labelAnimation.onEnd,
              }
            ]
          },
          {
            class: 'skillclass__controls_container',
            bottom: '20%',
            top: '60%',
            left: '0',
            right: '0',
            children: [
              {
                img: rootPath + '/button_get-course.png',
                class: 'skillclass__controls_button',
                action: StepStatus.custom,
                customAction: function() {
                  var evnt = document.createEvent('Event');
                  evnt.initEvent('clickGetCourse', true, true);
                  window.dispatchEvent(evnt);
                },
                width: '50%',
                onReady: buttonAnimation.onReady,
                onStart: buttonAnimation.onStart,
                onEnd: buttonAnimation.onEndValid,
                onclick: buttonAnimation.onClick,
                tag: "Get course",
              }
            ]
          },
        ]
      },
    ];
  };
}

const StepStatus = {
  question: "question",
  waiting: "waiting",
  invalid: "invalid",
  valid: "valid",
  none: undefined,
  custom: "custom",
};

export default LessonData;