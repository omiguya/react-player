function AudioChannel(audio_uri, volume) {
	this.audio_uri = audio_uri;
  this.resource = new Audio(audio_uri);
  this.resource.volume = volume;
}

AudioChannel.prototype.play = function() {
	this.resource.play();
}

function AudioSwitcher(audio_uri, num, volume) {
	this.channels = [];
	this.num = num;
	this.index = 0;

	for (var i = 0; i < num; i++) {
		this.channels.push(new AudioChannel(audio_uri, volume));
	}
}

AudioSwitcher.prototype.play = function() {
	this.channels[this.index++].play();
	this.index = this.index < this.num ? this.index : 0;
}

function SoundEffects(rootPath) {
  var intro   = new AudioSwitcher(rootPath + '/sf_magic.mp3', 1, 0.2);
  var tick    = new AudioSwitcher(rootPath + '/sf_collect.mp3', 2, 0.2);
  var success = new AudioSwitcher(rootPath + '/sf_level-start.mp3', 1, 0.2);
  var failure = new AudioSwitcher(rootPath + '/sf_error.mp3', 2, 0.2);
  var celebration = new AudioSwitcher(rootPath + '/sf_win--big.mp3', 1, 0.2);

  function playIntro() {
    intro.play();
  }

  function playTick() {
    tick.play();
  }

  function playSuccess() {
    success.play();
  }

  function playFailure() {
    failure.play();
  }

  function playCelebration() {
    celebration.play();
  }

  return Object({playIntro: playIntro, playTick: playTick, playSuccess: playSuccess, playFailure: playFailure, playCelebration: playCelebration});
}
export default  SoundEffects;