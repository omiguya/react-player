import styled, {keyframes} from 'styled-components';

const buttonGlow1 = keyframes`
    0%   { transform: scale(1.0); opacity: 0.8; }
    50%  { transform: scale(1.2); opacity: 1.0; }
    100% { transform: scale(1.0); opacity: 0.8; }
`
  
const buttonGlow2 = keyframes`
    0%   { transform: scale(1.2); opacity: 1.0; }
    50%  { transform: scale(1.0); opacity: 0.8; }
    100% { transform: scale(1.2); opacity: 1.0; }
`

const ButtonContainer = styled.div`
    position: absolute;
    display: flex;
    justify-content: center;
`;

const ButtonItem = styled.div`
    height: 100%;
    background-size: contain;
    background-repeat: no-repeat;
    background-position: center;
    position: relative;
    width: ${(props) => props.wd};
    &.skillclass__controls_title {
        div {
            background-position: center top;
        }
    }
    &.skillclass__controls_button {
        &:before {
            content: '';
            position: absolute;
            top: -35%;
            left: -35%;
            right: -35%;
            bottom: -35%;
            background-repeat: no-repeat;
            background-size: contain;
            background-position: center;
            opacity: 0.8;
            pointer-events: none;
        }
    }
    &.skillclass__controls_button--blue-glow {
        &:before {
            background-image: url(./assets/shine_blue.png);
            animation: ${buttonGlow1} 2s linear infinite;
        }
    }
    
`
const ButtonImg = styled.div`
    background-size: contain;
    background-repeat: no-repeat;
    background-position: center;
    width: ${(props) => props.wd};
    position: absolute;
    top: 0;
    left: 0;
    right: 0;
    bottom: 0;
    user-select: none;
    backface-visibility: hidden;
    
`


export {ButtonContainer, ButtonItem, ButtonImg}