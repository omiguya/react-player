import React, { Component } from 'react';

export default class SceneItem extends Component {

    getStyles = () => {
        const styles = {}
        const itemData = this.props.item
        if (itemData.children) {
            itemData.children.forEach(function(el) { 
                this.getStyles(el); 
            });
        }        

        if (itemData.top) styles.top = itemData.top;
        if (itemData.left) styles.left = itemData.left;
        if (itemData.right) styles.right = itemData.right;
        if (itemData.bottom) styles.bottom = itemData.bottom;
        if (itemData.width) styles.width = itemData.width;
        if (itemData.height) styles.height = itemData.height;
    }

    render(){
        console.log(this.props)
        return (
            <div></div>
        )

    }

}