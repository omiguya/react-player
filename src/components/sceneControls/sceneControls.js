import React, {PureComponent}  from 'react';
import {ButtonContainer, ButtonItem, ButtonImg} from './sceneStyle';

class SceneControls extends PureComponent {

    constructor(props){
        super(props);
        this.state = {
            scene: this.props.data.scene,
            isFound: false
        }
        
    }

    componentDidMount() {
        this.setState({
            scene: this.props.data.scene,
            isFound: this.props.stepIsFound
        });
    }

    onClickHandler = (status) => {
        this.props.onStepStatus(status)
    }
    
    render(){

        const data = this.props.data;  

        const Element = (data) => {
            
            const itemData = data.item;
            const styles = {}

            if (itemData.top) styles.top = itemData.top;
            if (itemData.left) styles.left = itemData.left;
            if (itemData.right) styles.right = itemData.right;
            if (itemData.bottom) styles.bottom = itemData.bottom;
            if (itemData.width) styles.width = itemData.width;
            if (itemData.height) styles.height = itemData.height;
            
            return (
                <ButtonContainer style={styles}>
                    {
                        itemData.children.map((item, i) => {
                            return(
                                <ButtonItem 
                                    
                                    key={i}
                                    wd={item.width}
                                    className={item.class}
                                    >
                                    <ButtonImg 
                                        onClick={()=> this.onClickHandler(item.action)} 
                                        style={{backgroundImage: `url(${item.img})` }} />
                                </ButtonItem>
                            ) 
                        })
                    }
                </ButtonContainer>
            )
        }

        if(this.props.stepIsFound){
            const createSceneElements = data.scene.map((item, i) => {
                return  <Element key={i} item={item} />
            });

            return (
                <div>{createSceneElements}</div>
            )
        } else {
            return null
            
        }
        
    }

}
export default SceneControls;