import styled from 'styled-components';

const TimerItem = styled.div`
    background: #fff;    
    position: absolute;
    padding: 4px;
    border-radius: 4px;
    color: #000;
`

export {TimerItem};