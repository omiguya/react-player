import React, { Component } from 'react';
import {TimerItem} from './timerStyle'

export default class Timer extends Component {
    render() {
        return (
            <TimerItem props={this.props.display}
                style={{ transform: `translateX(${this.props.position - 30}px)` }}>
                
                {this.props.time}
            </TimerItem>
        );
    }
}