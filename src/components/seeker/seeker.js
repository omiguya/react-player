import React, {Component} from 'react';
import {convertSecondsToHHMMss, percent} from '../../helpers'
import SceneList from '../scene/scene';
import {Progress, ProgressBg, ProgressBgCurrent, Knob, TimeCurrent, TimeTotal} from './seekerStyle';

export default class Seeker extends Component {
    constructor(props){
        super(props);
        this.state = {
            scale: 0,
            mousePosition: 0,
            display: false,
            time: 0
        }
    }

    onClick = (e) => {
        this.props.onSeek(percent(e.nativeEvent.layerX, e.currentTarget.offsetWidth));
    }

    render(){    
        return(
            <Progress
                id="player_progress"
                onClick={this.onClick}>

                <Knob style={{ transform: `translateX(${(this.props.currentPositionPercentual * this.props.width) / 100 - 15 }px)` }}/>
            
                <ProgressBg>
                    <ProgressBgCurrent style={{ transform: `scaleX(${this.props.currentPositionPercentual / 100})` }} />
                    <SceneList
                        onSceneClick={this.props.onSeek}
                        width={this.props.width}
                        data={this.props.data}
                        duration={this.props.duration}/>
                </ProgressBg>

                <TimeCurrent>{this.props.currentTime}</TimeCurrent>
                <TimeTotal>{ convertSecondsToHHMMss(this.props.duration)}</TimeTotal>

            </Progress>           
        )
    }
}