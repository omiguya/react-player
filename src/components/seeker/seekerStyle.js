import styled from 'styled-components';

const Progress = styled.div`
    position: absolute;
    height: 12px;
    left: 23px;
    right: 79px;
    bottom: 40px;
    cursor: pointer;
`;

const ProgressBg = styled.div`
    background: linear-gradient(180deg, #8797CF 0%, #A2B9ED 100%);
    border-radius: 12px;
    height: 12px;
    width: 100%;
    position: absolute;
    left: 0;
    top: 0;
    overflow: hidden;
`;

const ProgressBgCurrent = styled.div`
    background: linear-gradient(180deg, #F7C35E 0%, #F5B842 100%);
    box-shadow: inset 0px 1px 1px rgba(205, 137, 6, 0.2);
    position: absolute;
    width: 100%;
    height: inherit;
    transform: scaleX(0);
    transform-origin: top left;
`

const Knob = styled.div`
    background-image: url("data:image/svg+xml,%3Csvg xmlns='http://www.w3.org/2000/svg' fill='none' viewBox='0 0 32 32'%3E%3Cdefs/%3E%3Cg clip-path='url(%23clip0)'%3E%3Cpath fill='url(%23paint0_radial)' d='M.584 26.889c1.115 4.744 28.392 6.379 30.696 1.445 2.095-4.481.711-21.902-1.082-25.098-2.438-4.357-26.33-2.57-28.304.861C.054 7.301-.57 21.977.584 26.889z'/%3E%3Cpath fill='url(%23paint1_linear)' d='M.672 26.057c1.115 4.576 28.23 5.203 30.52.445 2.09-4.32.733-19.982-1.047-23.07C27.72-.77 3.916.909 1.948 4.215.115 7.3-.477 21.313.672 26.057z'/%3E%3Cpath fill='url(%23paint2_linear)' d='M1.008 25.648c1.088 4.437 27.619 5.058 29.869.445 2.042-4.189.705-19.435-1.041-22.427C27.458-.414 4.178 1.178 2.25 4.39.457 7.38-.12 21.05 1.008 25.649z'/%3E%3Cpath fill='%23fff' fill-opacity='.1' d='M30.346 26.758s-12.486 2.84-27.948.151c0 0 9.88-.398 14.347-1.625 7.583-2.078 11.606-.867 13.601 1.474z'/%3E%3Cpath fill='%23fff' fill-opacity='.67' d='M29.62 27.027c-.14-.51 1.095-2.824 1.411-2.81.316.008.034 1.526-.154 1.876-.188.35-1.182 1.19-1.256.934zM4.083 5.083s-2.1-.497-.427-1.366c1.672-.868 7.18-1.686 7.448-1.467.27.22-2.323 1.241-3.667 1.774-1.343.533-2.669 1.468-3.354 1.06z'/%3E%3Cpath fill='%23fff' fill-opacity='.1' d='M3.152 3.732c9.524-5.642 26.181-.982 26.181-.982s2.907 3.055-1.083 4.5C20.304 10.133 8.055 4.097 3.152 3.732z'/%3E%3Cpath fill='%23fff' fill-opacity='.67' d='M1.75 6.75c.067-.433.552-1.531.708-1.531.386-.24 1.071.402.75 1.74-.25 1.041-1.36 1.81-1.458 1.5-.115-.365-.083-1.167 0-1.709z'/%3E%3C/g%3E%3Cdefs%3E%3ClinearGradient id='paint1_linear' x1='16.169' x2='16.169' y1='29.786' y2='.933' gradientUnits='userSpaceOnUse'%3E%3Cstop stop-color='%23FFDC97' stop-opacity='.01'/%3E%3Cstop offset='1' stop-color='%23E19604'/%3E%3C/linearGradient%3E%3ClinearGradient id='paint2_linear' x1='16.169' x2='16.169' y1='29.275' y2='1.225' gradientUnits='userSpaceOnUse'%3E%3Cstop stop-color='%23FBB836'/%3E%3Cstop offset='1' stop-color='%23E8AE40'/%3E%3C/linearGradient%3E%3CradialGradient id='paint0_radial' cx='0' cy='0' r='1' gradientTransform='matrix(12.4974 0 0 25.4628 16.693 29.164)' gradientUnits='userSpaceOnUse'%3E%3Cstop stop-color='%23FABD49'/%3E%3Cstop offset='1' stop-color='%23EA9F0F'/%3E%3C/radialGradient%3E%3CclipPath id='clip0'%3E%3Cpath fill='%23fff' d='M0 0h32v32H0z'/%3E%3C/clipPath%3E%3C/defs%3E%3C/svg%3E");
    background-color: transparent;
    background-size: cover;
    background-position: center;
    width: 32px;
    height: 32px;
    cursor: pointer;
    position: absolute;
    top: -11px;
    left: 0;
    z-index: 2;
`;

const TimeCurrent = styled.div`
    position: absolute;
    left: 0;
    top: 20px;

`;

const TimeTotal = styled.div`
    position: absolute;
    right: 0;
    top: 20px;
`;


export { Progress, ProgressBg, ProgressBgCurrent, Knob, TimeCurrent, TimeTotal }