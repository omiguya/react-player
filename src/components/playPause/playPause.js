import React, {Component} from 'react';
import {PlayButton} from './playPauseStyles';

export default class PlayPause extends Component {
    
    onPlayHandler = () => {
        this.props.onPlayPause(!this.props.playing);
    }

    render(){
        
        return (
            <> 
                <PlayButton 
                    onClick={this.onPlayHandler}
                    isFound={this.props.stepIsFound}
                    isPlay={this.props.playing}/>
            </>
        )
    }

} 