import React, { Component } from 'react';
import SceneItem from './sceneItem';

export default class SceneList extends Component {
    render() {
        const {width, duration} = this.props;
        return (
            <>
            {
                this.props.data.map( (item, index) => {      
                    return <SceneItem 
                        key={index}
                        onSceneClick={this.props.onSceneClick}
                        end={item.end}
                        time={item.time}
                        width={width}
                        duration={duration}/>
                })
            }
            </>
        );
    }
}