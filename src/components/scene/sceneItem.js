import React, { Component } from 'react';
import {  convertSecondsToHHMMss, percent } from '../../helpers';
import {Item} from './sceneStyle';

export default class SceneItem  extends Component  {

    
    getSceneStyle(time, duration, endtime){
        return { 
            transform: `translateX(${(percent(time, duration) * this.props.width) / 100}px)`,
            width: `${ (percent( (endtime-time) , duration) * this.props.width) / 100 }px`
        }
    }

    onClick = (e) => {
        e.stopPropagation();
        this.props.onSceneClick(percent(this.props.time, this.props.duration));
    }
    
    render(){        
        
        return(
            <Item 
                onClick={this.onClick}
                style={this.getSceneStyle(this.props.time, this.props.duration, this.props.end)}/>
        )
    }
}

