import styled from 'styled-components';

const Item = styled.div`
    background: #f0b137;
    position: absolute;
    height: 12px;
    width: 10px;
    z-index: 2;
`

export { Item }