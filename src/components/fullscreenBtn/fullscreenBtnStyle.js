import styled from 'styled-components';

const iconEnter = `url("data:image/svg+xml,%3Csvg xmlns='http://www.w3.org/2000/svg' fill='%23fff' viewBox='0 0 18 18'%3E%3Cpath d='M1 12h3.6l-4 4L2 17.4l4-4V17h2v-7H1zM16 .6l-4 4V1h-2v7h7V6h-3.6l4-4z'%3E%3C/path%3E%3C/svg%3E")`;
const iconExit = `url("data:image/svg+xml,%3Csvg xmlns='http://www.w3.org/2000/svg' fill='%23fff' viewBox='0 0 18 18'%3E%3Cpath d='M10 3h3.6l-4 4L11 8.4l4-4V8h2V1h-7zM7 9.6l-4 4V10H1v7h7v-2H4.4l4-4z'%3E%3C/path%3E%3C/svg%3E")`;

const FullscreenBtn = styled.button`
    background-image: url("data:image/svg+xml,%3Csvg xmlns='http://www.w3.org/2000/svg' fill='none' viewBox='0 0 40 32'%3E%3Cdefs/%3E%3Cpath fill='url(%23paint0_radial)' d='M.723 26.889c1.38 4.744 35.124 6.379 37.974 1.445 2.592-4.481.88-21.902-1.338-25.098-3.016-4.357-32.573-2.57-35.016.861C.066 7.301-.707 21.977.723 26.889z'/%3E%3Cpath fill='url(%23paint1_linear)' d='M.831 26.057c1.38 4.576 34.924 5.203 37.758.445 2.584-4.32.906-19.982-1.297-23.07-3-4.203-32.448-2.524-34.882.782C.14 7.3-.59 21.313.83 26.057z'/%3E%3Cpath fill='url(%23paint2_linear)' d='M1.246 25.648c1.346 4.437 34.168 5.058 36.952.445 2.526-4.189.873-19.435-1.288-22.427-2.941-4.08-31.742-2.488-34.127.723C.565 7.38-.15 21.05 1.246 25.649z'/%3E%3Cpath fill='%23fff' fill-opacity='.1' d='M37.541 26.758s-15.447 2.84-34.575.151c0 0 12.223-.398 17.749-1.624 9.381-2.08 14.359-.868 16.826 1.473z'/%3E%3Cpath fill='%23fff' fill-opacity='.67' d='M36.644 27.027c-.174-.51 1.355-2.824 1.745-2.81.39.008.042 1.526-.191 1.876-.233.35-1.462 1.19-1.554.934zM5.051 5.083s-2.598-.497-.529-1.366c2.07-.868 8.883-1.686 9.215-1.467.333.22-2.875 1.241-4.537 1.774-1.662.533-3.301 1.468-4.149 1.06z'/%3E%3Cpath fill='%23fff' fill-opacity='.1' d='M3.899 3.732c11.782-5.642 32.39-.982 32.39-.982s3.595 3.055-1.34 4.5c-9.83 2.883-24.984-3.153-31.05-3.518z'/%3E%3Cpath fill='%23fff' fill-opacity='.67' d='M1.367 6.75c.052-.433.431-1.531.554-1.531.3-.24.836.402.585 1.74-.195 1.041-1.063 1.81-1.139 1.5-.09-.365-.065-1.167 0-1.709z'/%3E%3Cdefs%3E%3ClinearGradient id='paint1_linear' x1='20.003' x2='20.003' y1='29.786' y2='.933' gradientUnits='userSpaceOnUse'%3E%3Cstop stop-color='%23D0D9F1' stop-opacity='.58'/%3E%3Cstop offset='1' stop-color='%238191C5'/%3E%3C/linearGradient%3E%3ClinearGradient id='paint2_linear' x1='20.003' x2='20.003' y1='29.275' y2='1.225' gradientUnits='userSpaceOnUse'%3E%3Cstop stop-color='%236072AD'/%3E%3Cstop offset='1' stop-color='%2398ADE2'/%3E%3C/linearGradient%3E%3CradialGradient id='paint0_radial' cx='0' cy='0' r='1' gradientTransform='matrix(15.4609 0 0 25.4627 20.652 29.164)' gradientUnits='userSpaceOnUse'%3E%3Cstop stop-color='%2398ADE2'/%3E%3Cstop offset='1' stop-color='%23647CC2'/%3E%3C/radialGradient%3E%3C/defs%3E%3C/svg%3E");
    background-size: cover;
    background-repeat: no-repeat;
    background-position: center;
    background-color: transparent;
    border: 0;
    width: 40px;
    height: 32px;
    position: absolute;
    right: 23px;
    bottom: 31px;
    cursor: pointer;

    
    
    &::before {
        background-image: ${props => props.isToggled ? `${iconEnter}` : `${iconExit}`};

        content: '';
        display: block;
        position: absolute;
        left: 50%;
        top: 50%;
        transform: translate(-50%,-50%);

        width: 16px;
        height: 16px;

    }
`

export { FullscreenBtn }
