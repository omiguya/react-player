import React, {Component} from 'react';
import {FullscreenBtn} from './fullscreenBtnStyle';

export default class FullscreenToggle extends Component {

    render(){
        return (
            <>
                <FullscreenBtn
                    isToggled={this.props.fullscreen}
                    onClick={this.props.onToggleFullscreen}/>
            </>
        )
    }

}