import styled from 'styled-components';

const PlayerContainer = styled.div`
    max-width: 500px;
    margin: 0 auto;
    position: relative;

    font-family: -apple-system, BlinkMacSystemFont, 'Segoe UI', Roboto, Oxygen, Ubuntu, Cantarell, 'Open Sans', 'Helvetica Neue', sans-serif;
    font-size: 14px;
    font-weight: 700;
    color: #fff;

    ${props => {
        if(props.isFullscreen) {
            return `
                &::fullscreen { position: fixed; }
            `
        }
    }}

    video {
        max-width: 100%;
    }
`

const PlayerControls = styled.div`
    position: absolute;
    left: 0;
    right: 0;
    transition: all .2s ease;
    bottom: 0;
    opacity: ${props => props.isPlay ? `0` : `1`};
    visibility: ${props => props.isPlay ? `hidden` : `visible`};
    z-index: 2;
`
const VideoWrapper = styled.div`
    height: 100%;
    width: 100%;
    margin: auto;
    overflow: hidden;
    position: ${props => props.isToggled ? `relative` : `static`};
    video {
        display: block;
        width: 100%;
        height: 100%;
        &::-webkit-media-controls {
            display: none !important
        }

    }

`


export {PlayerContainer, PlayerControls, VideoWrapper};