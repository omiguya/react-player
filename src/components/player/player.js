import React, { Component } from 'react';
import PlayPause from '../playPause';
import {
    percent,
    convertSecondsToHHMMss} from '../../helpers';
import Seeker from '../seeker';
import {PlayerContainer, PlayerControls, VideoWrapper} from './playerStyle';
import FullscreenToggle from '../fullscreenBtn';
import SceneControls from '../sceneControls';

import errorSound from '../../data/sound/sf_error.mp3'
import successSound from '../../data/sound/sf_level-start.mp3'


const enterFullScreen = (element) => {
    if (element.requestFullscreen) {
        element.requestFullscreen();
    } else if (element.msRequestFullscreen) {
        element.msRequestFullscreen();
    } else if (element.mozRequestFullScreen) {
        element.mozRequestFullScreen();
    } else if (element.webkitRequestFullscreen) {
        element.webkitRequestFullscreen();
    }
}
  
const exitFullScreen = () => {
    if (document.exitFullscreen) {
        document.exitFullscreen();
    } else if (document.msExitFullscreen) {
        document.msExitFullscreen();
    } else if (document.mozCancelFullScreen) {
        document.mozCancelFullScreen();
    } else if (document.webkitExitFullscreen) {
        document.webkitExitFullscreen();
    }
}


export default class Player extends Component {

    constructor(props){
        super(props);
        this.state = {
            duration: 0,
            currentPositionPercentual: 0,
            playing: false,
            fullscreen: false,
            width: 0,
            currentTime: '00:00',
            path: this.props.data.assetsPath,
            sound: this.props.sound,
            data: this.props.data.getTimelineData(this.props.path, this.props.sound),
            stepToBeFound: false,
            currentStep: {},
            invalidAction: false,
            stepState: 'none'
        }
    }

    componentDidMount() {
        this.video.controls = false;
        this.video.addEventListener('timeupdate', this.onTimeUpdate.bind(this));
        this.video.addEventListener('loadedmetadata', this.onLoadMetadata.bind(this));
        this.video.addEventListener('seeked', this.onSeeked.bind(this));
        window.addEventListener('resize', this.onResize.bind(this));
        this.onResize();
    }

    onResize() {
        this.setState({ width: document.getElementById('player_progress').offsetWidth });
    }

    onLoadMetadata() {
        this.setState({ duration: this.video.duration });
    }

    onStepStatusClick = (status) => {
        
        const InvalidSound = new Audio(errorSound);
        const ValidSound = new Audio(successSound);

        this.setState({
            stepState: status
        })
        
        switch (status) {
            case 'invalid':
                
                InvalidSound.play();
                this.video.currentTime = this.state.currentStep.invalidAnswerTime[0]
                break;
            case 'valid':
                ValidSound.play();
                this.setState({
                    stepToBeFound: false,
                    currentStep: {}
                })
                this.video.currentTime = this.state.currentStep.validAnswerTime
                break;
            default: 
                console.log('error');
        }
    }

    repeatWaitingIfNeeded = (time, step) => {
        if (step.repeat && step.repeat.length === 2 && time >= step.repeat[1]) {
            this.video.currentTime = step.repeat[0];
        }
    }
    

    onTimeUpdate() {
        this.setState({ 
            currentPositionPercentual: percent(this.video.currentTime, this.video.duration),
            currentTime: convertSecondsToHHMMss(this.video.currentTime)
        });

        if (this.video.ended) {
            this.setState({
                playing: false,
                currentPositionPercentual: 0
            });
        }
        
        const data = this.state.data;
        const time = this.video.currentTime;
        
        for (let i = 0; i < data.length; i++) {
            const item = data[i];
            const timeStart = item.time;
            const timeEnd = item.end;

            if( time >= timeStart && time < timeEnd ) {
                this.setState({
                    stepToBeFound: true,
                    currentStep: item
                });
                break;
            }
        } 
        
        if (this.state.stepToBeFound) {

            const stepState = this.state.stepState;
            const currentStep = this.state.currentStep;
            const time = this.video.currentTime;
            
            switch (stepState) {
              case 'invalid':
                if ( currentStep.invalidAnswerTime && currentStep.invalidAnswerTime.length === 2 && time >= currentStep.invalidAnswerTime[1] ) {
                    this.setState({ stepState: 'waiting' })
                    this.repeatWaitingIfNeeded(time, currentStep);
                }
                break;
              case 'waiting':
                this.repeatWaitingIfNeeded(time, currentStep);
                break;

              default:
                if (time >= currentStep.time && time < currentStep.end) {
                    this.setState({ stepState: 'question' })
                }
                if (currentStep.repeat && currentStep.repeat.length === 2 && time >= currentStep.repeat[0] && time < currentStep.repeat[1]) {
                    this.setState({ stepState: 'waiting' })
                }
                if (currentStep.invalidAnswerTime && currentStep.invalidAnswerTime.length === 2 && time >= currentStep.invalidAnswerTime[0] && time < currentStep.invalidAnswerTime[1]) {
                    this.setState({ stepState: 'invalid' })
                }
                if (currentStep.validAnswerTime && time >= currentStep.validAnswerTime) {
                    this.setState({ stepState: 'valid' })
                }
                break;
            }
    
          } else {
            this.setState({
                currentStep: {},
                stepState: "none",
                stepToBeFound: false
            })
        }    
    }

    onSeeked = () => {
        const interval = setTimeout(() => {
            this.setState({ playing: true });
            this.video.play();
            clearTimeout(interval);
        }, 300);
    }
    
    seek = (percent) => {
        this.setState({ playing: false });
        this.video.pause();
        const interval = setTimeout(() => {
            this.video.currentTime = (percent * this.video.duration) / 100;
            clearTimeout(interval);
        }, 100);
    }

    sceneHandler = (stepState) => {
        this.setState({
            stepStatus: stepState
        });
    }
    
    onPlayPauseClick = (playing) => {
        this.setState({ playing: playing });

        if (this.state.playing) {
            this.video.pause();
        } else {
            this.video.play();
        }
    }

    onToggleFullscreenClick = () => {
        const fullscreen = this.state.fullscreen;
        const playerContainer = document.getElementById('playerContainer');
        
        if(!fullscreen) {
            enterFullScreen(playerContainer);
            this.setState ({ fullscreen: !this.state.fullscreen })
        } else {
            exitFullScreen();
            this.setState ({ fullscreen: !this.state.fullscreen })
        }
    }

    render(){
        
        return (
            <PlayerContainer 
                id="playerContainer"
                isFullscreen={this.state.fullscreen}>
                <VideoWrapper isToggled={this.state.fullscreen}>
                    <video 
                        className="player_container--element"
                        preload="auto"
                        playsInline
                        muted
                        controls={false}
                        ref={(video) => { this.video = video; }}>
                        <source src={this.props.data.videoSource} />
                    </video>
                </VideoWrapper>
        
                <div className="controlsContainer">
                    <SceneControls 
                        stepIsFound={this.state.stepToBeFound}
                        data={this.state.currentStep} 
                        onStepStatus={this.onStepStatusClick}/>
                    
                    <PlayPause 
                        stepIsFound={this.state.stepToBeFound}
                        playing={this.state.playing}
                        onPlayPause={this.onPlayPauseClick}/>

                    <PlayerControls
                        isPlay={this.state.playing}
                        >    
                        <Seeker
                            data={this.state.data}
                            currentTime={this.state.currentTime}
                            onSeek={this.seek}
                            duration={this.state.duration}
                            width={this.state.width}
                            currentPositionPercentual={this.state.currentPositionPercentual}/>

                        <FullscreenToggle
                            fullscreen={this.state.fullscreen}
                            onToggleFullscreen={this.onToggleFullscreenClick}/>
                    </PlayerControls>
                </div>
            </PlayerContainer>
        )
    }
    

}
