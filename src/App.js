import React from 'react';
import Player from './components/player';
import LessonData from './data/lesson-data.js';
import SoundEffects from './data/sound-effects.js' /* remove */

const lesson = new LessonData();
const path = lesson.assetsPath;

function App() {
	return (
		<div className="App">
			<Player
				path={path} 
				data={lesson}
				sound={SoundEffects} /* remove *//>
		</div>
	);
}

export default App;
